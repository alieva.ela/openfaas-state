import com.kubernetes.zookeeper
import com.kubernetes.zookeeper.Persisted
import com.kubernetes.zookeeper.Persisted.PersistedValue

def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    seenCount = PersistedValue.of("zoo-count")
    if req == "":
    	seen = seenCount.getOrDefault(0);
    	seenCount.set(seen + 1);
    else:
    	req = int(req)
    	seen = seenCount.getOrDefault(req);
    	seenCount.set(seen + 1);

    return seen
